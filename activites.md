---
title: "Activités"
order: 1
in_menu: true
---
L’association Amicale Laïque de Clairac propose un atelier informatique le jeudi après-midi de 14H30 à 16H30 à la salle du Tiers-Lieu.

# Ce que vous y trouverez

En venant avec votre outil informatique, tablette ordinateur ou même téléphone portable nous essaierons de répondre à vos questions relatives à l'utilisation de cet outil.

   - Organiser sa messagerie,
   - Utiliser les outils logiciels LibreOffice
   - Apprendre à faire du ménage sur son stockage
   - Et tout ce que vous aurez envie d'apprendre 