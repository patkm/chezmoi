---
layout: default
title: Articles
permalink: /articles/
---
<h1>
  Articles
</h1>

{% for post in site.posts %}
<article class="blog-item">
  <h2>
    <a href="{{post.url | relative_url}}"> {{ post.title }} </a>
  </h2>

  <a href="{{post.url | relative_url}}"> Lire l'article ➞ </a>
</article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/FairEmail.png">
    </div>
    <div>
      <h2>FairEmail</h2>
      <p>Client Android de messagerie, minimaliste privilégiant la lecture et la rédaction, orienté sécurité.</p>
      <div>
        <a href="https://framalibre.org/notices/fairemail.html">Vers la notice Framalibre</a>
        <a href="https://email.faircode.eu/">Vers le site</a>
      </div>
    </div>
  </article>



<hr />
{% endfor %}
